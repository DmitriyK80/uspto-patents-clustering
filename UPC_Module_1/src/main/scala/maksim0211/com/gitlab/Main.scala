package maksim0211.com.gitlab

import maksim0211.com.gitlab.Loader.loadFilesToResourcesDir

object Main{

  def main(args: Array[String]): Unit = {
    //Принимаются параметры
    val parameters = new Parameters(args)
    //Загружаются файлы(архивы) в директорию
    loadFilesToResourcesDir(parameters)
  }
}
