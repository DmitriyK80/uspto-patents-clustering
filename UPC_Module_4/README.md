# UPC

### UPC_Module_4

### Репозиторий проекта по разработке программного продукта для кластеризации патентного массива и формирования патентного ландшафта. Модуль 4.

### Используется Python 3.8.0

### Функционал

Модуль трансформирует данные для обучения LDA модели и визуализирует результаты (в jupyter-notebook).
