import org.apache.spark.sql.{SaveMode, SparkSession}
import PrepareData.getNecessaryDataset

object TestDatasetBuilder{
  def main(args: Array[String]): Unit = {

  val spark = SparkSession
    .builder()
    .master("local")
    .appName("VKR_MODULE_3_1_tstdtstbldr")
    .getOrCreate()

  // Создание тестовых датасетов для проверки работы 4-го модуля
  val neededDataset11000 = getNecessaryDataset(args, spark, Array(1, 11, 12, 13, 15, 21), "test",5000)

  neededDataset11000
    .repartition(1)
    .write
    .mode(SaveMode.Overwrite)
    .parquet("~\\dataset_1_11_12_13_15_21_5k")

  }
}
