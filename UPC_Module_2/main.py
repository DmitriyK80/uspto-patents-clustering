import datetime
import os
import csv
import traceback
from bs4 import BeautifulSoup as bs


def split(name):
    with open(name, "r") as file:
        # Читаем файл патентов и разбиваем по `delimiter`
        delimiter = """<?xml version="1.0" encoding="UTF-8"?>\n<!DOCTYPE us-patent-grant SYSTEM"""
        content = file.readlines()
        content = "".join(content)
        splitted = content.split(delimiter)
        cnt = 0
        name_of_dir = name[0:len(name) - 4]
        # Cоздаем папку для данных патентов за неделю
        os.mkdir(name_of_dir)
        while cnt < len(splitted):
            file = open(name_of_dir + "\\part-" + str(cnt) + ".xml", "w+")
            # Записываем xml файл в данную папку
            file.write(splitted[cnt])
            file.close()
            cnt = cnt + 1
        print("split is successful")
        return name_of_dir


def highlight_necessary_info(name_of_dir):
    cnt_err = 0  # Счетчик патентов, которые не могли быть распарсены
    cnt_ok = 0  # Счетчик патентов, которые были добавлены в итоговый файл csv
    cnt = 0  # Счетчик всех патентов в папке за неделю
    week = name_of_dir[len(name_of_dir) - 6:len(name_of_dir)]  # Получение недели из названия директории
    list = os.listdir(name_of_dir)  # Список файлов патентов в папке за неделю
    number_files = len(list) - 2  # Число данных файлов
    print("Start: " + name_of_dir[len(name_of_dir) - 10:] + " - " + str(datetime.datetime.now().time()))
    while cnt < number_files:
        with open(name_of_dir + "\\part-" + str(cnt) + ".xml", "r") as file:
            # Читаем с директории файл патента
            content = file.readlines()
            content = "".join(content)
            bs_content = bs(content, "lxml")
            lastmg = str("")  # Для дедупликации одинаковых патентов по main-group
            text = ""
            try:
                # Ищем данные поля в xml файле патента
                for element in bs_content.find("us-bibliographic-data-grant").find("classifications-ipcr").find_all(
                        "classification-ipcr"):
                    section = element.find("section").text
                    classname = element.find("class").text
                    subc = element.find("subclass").text
                    mg = element.find("main-group").text
                    # Если файл патента принадлежит интересующей нас категории
                    '''and subc == "F" '''
                    if section == 'A' and classname == '61' and lastmg != str(mg):
                        # Запоминаем main-group
                        lastmg = str(mg)
                        # Если патент по данной категории не приходит второй раз (у одного патента
                        # может быть несколько категорий сразу)
                        if text == "":
                            # Выделяется текст описания патента
                            for el in bs_content.find("description").find_all("p"):
                                tmp = el.text
                                # Вплоть до начала описания иллюстраций патента
                                if str(tmp).find("FIG. ") == -1:
                                    text += tmp
                                else:
                                    break
                        # Инкрементирующим образом записывается в csv файл
                        with open(os.curdir + "\\test_predzatshita.csv", mode='a') as res_file:
                            writer = csv.writer(res_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                            writer.writerow([week, cnt, subc, mg,
                                             text.encode("cp1251", errors="ignore").decode("cp1251", errors="ignore")])
                            # Патент записан в сырой датасет
                            cnt_ok = cnt_ok + 1
            except AttributeError as e:
               pass
            except Exception as e:
                print('Ошибка:\n', traceback.format_exc())
                print("error: part-" + str(cnt))
                # При возникновении ошибки inc счетчик ошибок
                cnt_err = cnt_err + 1
            finally:
                cnt = cnt + 1
                if (cnt % 100 == 0):
                    print("cnt/cnt_ok: " + str(cnt) + "/" + str(cnt_ok))
    print("End: " + name_of_dir[len(name_of_dir) - 10:] + " - " + str(datetime.datetime.now().time()))


if __name__ == "__main__":
    # Для парсинга за 3 года
    year = 2022
    while year != 2023:
        # Укажите вашу директорию из прошлого модуля
        name = "C:\\Users\\sawel\\Documents\\DEF\\upc\\UPC_Module_1\\src\\main\\dataset" + str(year)
        list = os.listdir(name)  # dir is your directory path
        cnt_file = 0
        need_split_large_batch = True
        while cnt_file < len(list):
            if need_split_large_batch == True:
                # Выполнить разбиение файла с патентами за неделю
                name_of_dir = split(name + "\\" + list[cnt_file])
            else:
                # Если файлы были уже разбиты, пропустить операцию split
                name_of_dir = name + "\\" + list[cnt_file]
            # Составить csv файл из патентов
            highlight_necessary_info(name_of_dir)
            cnt_file = cnt_file + 1
        year = year + 1